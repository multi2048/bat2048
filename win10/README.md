# CMD2048 for Windows 10 and 11
This one uses ASCII escape sequences to move the cursor, as such it won't work
on earlier Windows versions. The console host did not support ASCII sequences
before Windows 10.
