@ECHO OFF

REM :: Works under Windows 10 and 11. ConHost of earlier versions does not
REM :: support ANSI escape sequences.

SETLOCAL
SETLOCAL EnableDelayedExpansion
TITLE cmd2048

REM # Initialise the board with zeroes
FOR /l %%r in (0,1,3) do (
FOR /l %%c in (0,1,3) do (
SET board[%%r][%%c]=0
))
SET score=0
SET turns=0

CALL :PaintBoard
:MainLoop
Call :PaintState
CHOICE /c aswdhjkl4286qx /n
IF %errorlevel%==1 CALL :Push_lt
IF %errorlevel%==5 CALL :Push_lt
IF %errorlevel%==9 CALL :Push_lt
IF %errorlevel%==2 CALL :Push_dn
IF %errorlevel%==6 CALL :Push_dn
IF %errorlevel%==10 CALL :Push_dn
IF %errorlevel%==3 CALL :Push_up
IF %errorlevel%==7 CALL :Push_up
IF %errorlevel%==11 CALL :Push_up
IF %errorlevel%==4 CALL :Push_rt
IF %errorlevel%==8 CALL :Push_rt
IF %errorlevel%==12 CALL :Push_rt
IF %errorlevel%==13 GOTO End
IF %errorlevel%==14 GOTO End
GOTO :MainLoop

:PaintBoard
CLS
REM # Could just have read a text file and printed that, but I want to try
REM # keeping everything in one file.
ECHO.
ECHO   Score:
ECHO   Turns:
ECHO.
ECHO   +----+----+----+----+
ECHO   ^|    ^|    ^|    ^|    ^|
ECHO   +----+----+----+----+
ECHO   ^|    ^|    ^|    ^|    ^|
ECHO   +----+----+----+----+
ECHO   ^|    ^|    ^|    ^|    ^|
ECHO   +----+----+----+----+
ECHO   ^|    ^|    ^|    ^|    ^|
ECHO   +----+----+----+----+
ECHO.
ECHO   Controls: ASWD, HJKL, 4286
ECHO       Quit: Q, X
EXIT /b

:PaintState
REM # Start with score and turns
REM # These always increase, so there's no need to blank them out first.
ECHO [2;10 %score%
ECHO [3;10 %turns%
SETLOCAL
REM # Fill in new values on the board.
REM # Need to be blanked first because the values can decrease.
FOR /l %%r in (0,1,3) do (
FOR /l %%c in (0,1,3) do (
SET /a tr=6+%%r*2
SET /a tc=4+%%c*5
ECHO [!tr!;!tc!H    [1C
ECHO [!tr!;!tc!H!board[%%r][%%c]!
)
)
ENDLOCAL
ECHO [17;0H
EXIT /b

:AddNum
REM # Get all cells that are zero.
FOR /l %%r in (0,1,3) do (
FOR /l %%c in (0,1,3) do (
IF !board[%%r][%%c]! EQU 0 (
SET "tmp=!tmp! %%r_%%b"
)
)
)
REM # Count the acquired cells.
REM # Start zerocount at 1 to account for RANDOM calculation being floored.
SET zerocount=1
FOR %%n in (%tmp%) do (
SET /a zerocount+=1
)
REM # If there are no zero-cells, we can't add a new number, so don't try to.
IF %zerocount% GTR 1 (
REM # Get a random number between 0 and the count.
SET _newval=(%RANDOM%*10/32768)+1
IF %_newval% EQU 1 (
SET _newval=4
) ELSE (
SET _newval=2
)
SET _rand=(%RANDOM%*%zerocount%/32768)
REM # Pick the random cell
SET m=0
FOR %%n in (%tmp%) do (
SET /a m+=1
IF !m! EQU %_rand% (
SET tc=%%n
GOTO AddNum_aqd_cell
)
)
:AddNum_aqd_cell
REM # Only place a value in the cell if we actually have a cell to place it in.
IF %tc% SET board[%tc:~1,1%][%tc:~3%]=%_newval%
)
REM # Empty a bunch of temp variables
SET _rand=
SET _newval=
SET m=
SET tc=
SET tmp=
SET zerocount=
EXIT /b

:Mirror
FOR /l %%r in (0,1,3) do (
SET tmp=!board[%%r][0]!
SET board[%%r][0]=!board[%%r][3]!
SET board[%%r][3]=!tmp!
SET tmp=!board[%%r][1]!
SET board[%%r][1]=!board[%%r][2]!
SET board[%%r][2]=!tmp!
)
SET tmp=
EXIT /b

:Invert
FOR /l %%r in (0,1,2) do (
FOR /l %%c in (0,1,3) do (
IF %%c GTR %%r (
SET tmp=!board[%%r][%%c]!
SET board[%%r][%%c]=!board[%%c][%%r]!
SET board[%%c][%%r]=!tmp!
)
)
)
SET tmp=
EXIT /b

:Push_lt
SET changed=0
FOR /l %%r in (0,1,3) do (
SET temp_c=0
SET combined=0
FOR /l %%c in (1,1,3) do (
REM # Checks:
REM ## combined==0, temp_cell-1_val==curr_cell_val -> combine
REM ## Else -> append, inc temp_cell_num
IF %combined%==0 (
IF %temp_c% GTR 0 (
SET /a check_c=%temp_c%-1
IF !board[%%r][%check_c%]!==!board[%%r][%%c]! (
SET append=0
) ELSE (
SET append=1
)
) ELSE (
SET append=1
)
) ELSE (
SET append=1
)
IF %append%==1 (
SET board[%%r][%temp_c%]=!board[%%r][%%c]!
SET combined=0
SET /a temp_c+=1
) ELSE (
SET /a board[%%r][%check_c%]=board[%%r][%check_c%]*2
SET combined=1
)
)
)
REM # Increase turn count and add another number if board changed
IF %changed%==1 (
SET /a turns+=1
CALL :AddNum
)
SET temp_c=
SET check_c=
SET curr_c=
SET combined=
SET changed=
EXIT /b

:Push_rt
CALL :Mirror
CALL :Push_lt
CALL :Mirror
EXIT /b

:Push_up
CALL :Invert
CALL :Push_lt
CALL :Invert
EXIT /b

:Push_dn
CALL :Invert
CALL :Push_rt
CALL :Invert
EXIT /b

:End
FOR /l %%r in (0,1,3) do (
FOR /l %%c in (0,1,3) do (
SET board[%%r][%%c]=
))
SET score=
SET turns=
ENDLOCAL
