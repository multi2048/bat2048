# bat2048
Implementation of the game 2048 in DOS and Windows Batch and CMD files. Just
to see how far I can take it. Windows XP and above shouldn't be much of a
problem, but DOS and earlier Windows will be an interesting challenge.

# win10
A version for Windows 10 and above was ready first, as the command prompt there
is slightly more familiar from a *nix point-of-view, particilarly in that it
supports ASCII escape sequences. Also, it inherits RNG and arithmetic from
earlier NT's.

# win2000
A version for Windows 2000 and newer. Inherits the maths knowledge of WinNT4,
and adds the %random% variable for, well, random numbers.

# winnt4
For Windows NT 4. Does know basic arithmetic, but does not have a
built-in random-number-generator.

# winnt3
I don't know if I'll try this one at all. No arithmetic, no RNG, but it's NT,
so the quirks are different from those of DOS.

# dos
A version for DOS and DOS-based Windows. No random-number-generator, no
arithmetic (everything's a string), a heap of fun. May work on DOS 5, but
probably requires DOS 6.
